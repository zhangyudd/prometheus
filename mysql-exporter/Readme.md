# 数据库监控系统
## Lepus天兔开源企业级数据库监控系统



## Percona Monitoring and Management



## mysql-exporter监控多个mysql实例
[参考](https://www.modb.pro/db/565900)

下载源码编译

```
https://github.com/prometheus/mysqld_exporter/archive/refs/heads/main.zip
```

mysql配置权限

```
CREATE USER 'exporter'@'192.168.%' identified by 'exporter';
GRANT PROCESS, REPLICATION CLIENT, SELECT ON *.* TO 'exporter'@'192.168.%';
GRANT SELECT ON mysql.* TO 'exporter'@'192.168.%';
GRANT SELECT ON performance_schema.* TO 'exporter'@'192.168.%';
```

配置文件（用户名密码）

```
# cat my.cnf 
[client]
user = exporter
password = exporter

[client-exporter1]
user = exporter1
password = exporter1

[client-exporter2]
user = exporter2
password = exporter2
```

启动命令

```
./mysqld_exporter --config.my-cnf=my.cnf --web.listen-address=:42002 
```

promethus配置

静态：

```
    - job_name: 'mysql'
      metrics_path: /probe
      static_configs:
        - targets:
          - 192.168.168.11:6666
          - 192.168.168.12:6666
          labels:
            auth_module: client
            project: ca
        - targets:
          - 192.168.168.11:23750
          - 192.168.168.14:23750
          labels:
            auth_module: client-exporter2
      relabel_configs:
        - source_labels: [__address__]
          target_label: __param_target
        - source_labels: [__param_target]
          target_label: instance
        - target_label: __address__
          replacement: 192.168.168.11:42002
        - source_labels: [auth_module]
          target_label: __param_auth_module
        - action: labeldrop
          regex: auth_module
```

基于文件发现

```
    - job_name: 'mysql'
      metrics_path: /probe
      file_sd_configs:
        files:
        - "/prometheus/fileDiscover/mysql.yaml"
      relabel_configs:
        - source_labels: [__address__]
          target_label: __param_target
        - source_labels: [__param_target]
          target_label: instance
        - target_label: __address__
          replacement: 192.168.168.11:42002
        - source_labels: [auth_module]
          target_label: __param_auth_module
        - action: labeldrop
          regex: auth_module
```



```
        - targets:
          - 192.168.168.11:6666
          - 192.168.168.12:6666
          labels:
            auth_module: client
            project: ca
        - targets:
          - 192.168.168.11:23750
          - 192.168.168.14:23750
          labels:
            auth_module: client-exporter2
```

