## 原理
Metrics-Server通过kubelet获取监控数据。

## 部署
1. 查看k8s对应的版本 https://github.com/kubernetes-sigs/metrics-server/
2. 下载对应的image版本
3. 下载对应的部署文件
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.7.1/components.yaml

4. 修改部署文件参数
```
      - args:
        - --cert-dir=/tmp
        - --secure-port=10250
        - --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname
        - --kubelet-use-node-status-port
        - --metric-resolution=15s
        - --kubelet-insecure-tls  # 添加忽略证书
```
5. 部署验证 
```
kubectl top nodes

```
