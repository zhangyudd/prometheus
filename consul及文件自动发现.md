## blackbox通过file_sd_configs自动发现

参考：https://blog.csdn.net/weixin_45717886/article/details/125976668

### 安装

* 安装blackbox组件
* 通过配置Prometheus添加file_sd_configs监控端口

### 配置Prometheus监控端口

```yaml
  - job_name: "PORT_check_198"
    metrics_path: /probe
    params:
      module: [tcp_connect]
    file_sd_configs:
    - refresh_interval: 10s
      files:
      - "/prometheus/blackbox/port_check.yml"
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: blackbox:9115
```

port_check.yml（添加监测内容不需要重新加载Prometheus）

```yaml
- targets:
  - zunshang:6379
  - zunshang:35050
  labels:
    company: 'feitian'
    env: 'pro'
    project: '樽商'
    
- targets:
  - 127.0.0.1:8004
  - 127.0.0.1:8005
  labels:
    company: 'feitian'
    env: 'pro'
    project: 'ai'   
```

grafana添加blackbox模板

### 配置Prometheus监控url

```yaml
  - job_name: "HTTP_check"
    metrics_path: /probe
    params:
      module: [http_2xx]
    file_sd_configs:
    - refresh_interval: 10s
      files:
      - "/prometheus/blackbox/http_check.yml"
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: blackbox:9115
```

http_check.yml（添加监测内容不需要重新加载Prometheus）

```yaml
- targets:
  - https://zsdev.jinzunft.com
  - https://zs.jinzunft.com
  labels:
    project: '樽商'
    company: 'web'
    env: 'pro'
```

### 配置Prometheus监控ping（测试未发现指标）

建议采用自研脚本通过pushgateway推送指标（Prometheus文档记录）

1. shell 脚本获取指标

2. smokeping + shell/python 脚本 + pushgateway

```yaml
## 下面的内容是需要新添加到配置文件里面，主要用于测试服务器的连通性
  - job_name: 'PING_check'
    scrape_interval: 3s
    metrics_path: /metrics
    params:
      module: [icmp] #ping
    static_configs:
      - targets: ['ipaddress']   # 在配置是需要将ipaddress 设置为需要监控IP
        labels:
          group: '电信泉州'
    relabel_configs:
      - source_labels: [__address__]
        regex: (.*)(:80)?
        target_label: __param_target
        replacement: ${1}
      - source_labels: [__param_target]
        regex: (.*)
        target_label: ping
        replacement: ${1}
      - source_labels: []
        regex: .*
        target_label: __address__
        replacement: blackbox:9115   # 上面的9115是blackbox程序的启动端口
————————————————
版权声明：本文为CSDN博主「Richard-hao」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/weixin_42168046/article/details/102799993
```

ping 指标

```shell
max_over_time(probe_duration_seconds{job=~"blackbox_exporter.*_icmp"}[1m]) > 1

probe_success
```



### 配置Prometheus监控node节点

```yaml
- job_name: k8s1-node-exporter
  file_sd_configs:
  - files:
    - '/prometheus/node-exporter/node.yaml'
```

node.yaml

```yaml
- targets:
  - 192.168.12.107:9100
  - 192.168.12.108:9100
  labels:
    company: 'web'
    env: 'pro'
    project: 'a'
```





### grafana图表添加标签筛选

1. variables 修改

name: project

type: query

label: 项目名（自定义显示名称）

query：label_values(project)



2. 修改图表"metrics browser"

例如：probe_success 改为 probe_success{project=~"$project",job=~"$job"}

如果还有其他标签，逗号隔开即可



## promethus 基于consul 自动发现

参考： https://zhuanlan.zhihu.com/p/591545329

### 安装

* 运行单节点consul

```shell
docker run -d -ti  --name=consul --hostname consul-container -p 8500:8500  consul
```

### 自动发现node-exporter

* 注册节点

```shell
id="bjmiddle-waihu"
svcName="node-expoter-waihu"
agentAddress="172.30.10.238"
agentPort=9100
consulInterAddr="http://172.30.20.167:8500/v1/agent/service/register"

curl -X PUT -d '{
  "id": "'"$id"'",
  "name": "'"$svcName"'",
  "address": "'"$agentAddress"'",
  "port":'"$agentPort"',
  "tags": ["'"$svcName"'"],
  "meta":{"idc":"aliyun"},   # 自动打标签
  "checks": [{"http": "http://'"$agentAddress"':'"$agentPort"'","interval": "5s"}]}' $consulInterAddr
```

* prometheus 配置consul自动发现

```
    - job_name: 'node-expoter-waihu'
      consul_sd_configs:
      - server: 172.30.20.167:8500
        services: ['node-expoter-waihu']	
```

### 自动发现blackbox对象

```
consulInterAddr="http://172.30.20.167:8500/v1/agent/service/register"

curl -X PUT -d '{
  "id": "qc-web",
  "name": "http_200_check",
  "Meta": {
    "target": "https://qianchenglaw.com/"
  }
}' $consulInterAddr
```

* prometheus 配置consul自动发现

```
    - job_name: 'http_200_check'
      scrape_interval: 300s
      scrape_timeout: 15s
      metrics_path: /probe
      params:
        module: [http_2xx]
      consul_sd_configs:
        - server: 172.30.20.167:8500
      relabel_configs:
        - source_labels: [__meta_consul_service_metadata_target]
          regex: ""
          action: drop
        - source_labels: [__meta_consul_service_metadata_target]
          target_label: __param_target
        - source_labels: [__meta_consul_service_metadata_target]
          target_label: instance
        - target_label: __address__
          replacement: blackbox:9115
```

### 自动发现process_exporter对象

```
    - job_name: 'process-exporter'
      consul_sd_configs:
      - server: 172.30.20.167:8500
        services: ['process-exporter']
		
		

#!/bin/bash
service_name="process-exporter"
instance_id="ca-120.46.188.49"
ip="120.46.188.49"
port="9256"
 
curl -X PUT -d '{"id": "'"$instance_id"'","name": "'"$service_name"'","address": "'"$ip"'","port": '"$port"',"tags": ["'"$service_name"'"],"checks": [{"http": "http://'"$ip"':'"$port"'","interval": "5s"}]}' http://172.30.20.167:8500/v1/agent/service/register
```



### 删除自动发现的对象

*  删除发现的节点

```shell
curl -X PUT 172.30.20.167:8500/v1/agent/service/deregister/test-db
```

### 备份还原

* 备份还原

```shell
ts=$(date +%Y%m%d%H%M)

#备份
consul snapshot save consul_state_${ts}.snap
#查看备份
consul snapshot inspect consul_state_${ts}.snap
#还原备份
consul snapshot restore consul_state_${ts}.snap
```



### 监控consul

**安装**

```
docker run -tid --restart=always  -p 9107:9107 --name consul-expoter prom/consul-exporter:latest --consul.server=172.30.20.167:8500
```

**prometheus配置**

```
  - job_name: consul-exporter
    static_configs:
      - targets: ['172.30.20.167:9107']
```

**告警**

```
groups:
- name: consul
  rules:
  - alert: ConsulDown
    annotations:
      description: |-
        Consul instance is down
          VALUE = {{ $value }}
          LABELS: {{ $labels }}
      summary: Consul down (instance {{ $labels.instance }})
    expr: consul_up == 0
    for: 5m
    labels:
      severity: critical
```

