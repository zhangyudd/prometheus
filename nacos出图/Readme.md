# nacos 相关数据出图

## nacos暴露metrics数据
1. 访问看能否得到metrics数据（http://xx.xx.xx.xx:8848/nacos/actuator/prometheus）
2. 未访问到数据手动暴露数据
```
cd /workspace/nacos/conf
vim application.properties
management.endpoints.web.exposure.include=*
```

## prometheus 采集nacos数据
vi prometheus.yaml
```
  - job_name: 'nacos-cluster'
    scrape_interval: 60s
    metrics_path: '/nacos/actuator/prometheus'
    static_configs:
      - targets:
         - nacos-0.nacos-headless.dispute-pro
         - nacos-1.nacos-headless.dispute-pro
         - nacos-2.nacos-headless.dispute-pro
```

## grafana 配置图标