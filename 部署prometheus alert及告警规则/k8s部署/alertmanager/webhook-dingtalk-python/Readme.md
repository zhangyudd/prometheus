## 钉钉客户端添加机器人

## 打docker镜像
由于钉钉无法直接接收alertmanager传入的数据，需要一个接口作为中转

## altermanager配置
```
  config:
    global:
      resolve_timeout: 2m
    route:
      group_by: ['job']
      group_wait: 30s
      group_interval: 2m
      repeat_interval: 12h
      receiver: 'webhook'
      routes:
      - match:
          alertname: DeadMansSwitch
        receiver: 'webhook'
    receivers:
    - name: 'webhook'
      webhook_configs:
      - url: http://webhook-dingtalk/dingtalk/send/
        send_resolved: true
```
