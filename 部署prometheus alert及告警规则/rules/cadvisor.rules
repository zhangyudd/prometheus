groups:
- name: container
  rules:
  - alert: ContainerKilled
    expr: time() - container_last_seen{image!=""} > 60
    for: 1m
    labels:
      severity: warning
    annotations:
      summary: Container killed (instance {{ $labels.instance }})
      description: "A container has disappeared\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"

  - alert: ContainerAbsent
    expr: absent(container_last_seen)
    for: 1m
    labels:
      severity: warning
    annotations:
      summary: Container absent (instance {{ $labels.instance }})
      description: "一个容器消失达2分钟\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"

  - alert: 容器cpu使用率大于80%
    expr: (sum(rate(container_cpu_usage_seconds_total{name!=""}[3m])) BY (instance, name) * 100) > 80
    for: 5m
    labels:
      severity: warning
    annotations:
      summary: Container CPU usage (instance {{ $labels.instance }})
      description: "Container CPU usage is above 80%\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
  # See https://medium.com/faun/how-much-is-too-much-the-linux-oomkiller-and-used-memory-d32186f29c9d

  - alert: ContainerMemoryUsage
    expr: (sum(container_memory_working_set_bytes{name!=""}) BY (instance, name) / sum(container_spec_memory_limit_bytes > 0) BY (instance, name) * 100) > 80
    for: 5m
    labels:
      severity: warning
    annotations:
      summary: Container Memory usage (instance {{ $labels.instance }})
      description: "Container Memory usage is above 80%\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"

  - alert: 容器内存大于1G
    expr: sum(container_memory_rss{name=~".+"}) by (name,instance) > 1000000000 
    for: 2m
    labels:
      severity: warning
    annotations:
      description:  "Container memory over 1000M alarm"

   # - alert: ContainerVolumeUsage
   #  expr: (1 - (sum(container_fs_inodes_free{name!=""}) BY (instance) / sum(container_fs_inodes_total) BY (instance))) * 100 > 80
   #  for: 10m
   #  labels:
   #    severity: warning
   #  annotations:
   #    summary: Container Volume usage (instance {{ $labels.instance }})
   #    description: "Container Volume usage is above 80%\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"

  - alert: ContainerVolumeIoUsage
    expr: (sum(container_fs_io_current{name!=""}) BY (instance, name) * 100) > 80
    for: 2m
    labels:
      severity: warning
    annotations:
      summary: Container Volume IO usage (instance {{ $labels.instance }})
      description: "容器存储IO使用率，Container Volume IO usage is above 80%\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"

  - alert: ContainerHighThrottleRate
    expr: rate(container_cpu_cfs_throttled_seconds_total[3m]) > 1
    for: 2m
    labels:
      severity: warning
    annotations:
      summary: Container high throttle rate (instance {{ $labels.instance }})
      description: "Container is being throttled\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"


  - alert: Container-network-output-alarm 
    expr: sum by (name,instance) (irate(container_network_receive_bytes_total{name=~".+",image!=""}[3m])) /1024/1024 >50 
    for: 1m
    labels:
      severity: warning
    annotations:
      description:  "Warning of container output bandwidth greater than 50M"
  - alert: Container-network-input-alarm 
    expr: sum by (name,instance) (irate(container_network_transmit_bytes_total{name=~".+",image!=""}[3m])) /1024/1024 >50 
    for: 1m
    labels:
      severity: warning
    annotations:
      description:  "Warning of container input bandwidth greater than 50M"

  - alert: 容器占用磁盘空间大于100G
    expr: sum(container_fs_usage_bytes{name!=""}) by (name) /1024/1024/1024 > 100
    for: 1m
    labels:
      severity: warning
    annotations:
      description:  "容器{{ $labels }}占用磁盘空间大于100G"