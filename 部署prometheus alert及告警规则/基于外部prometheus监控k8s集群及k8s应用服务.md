# 基于外部prometheus监控k8s 集群及k8s应用服务

监控k8s有多种方式

1. 联邦模式
2. apiserver远程获取数据，这种方式适合数据量较少的场景

## 监控部署的组件

1. Prometheus
2. node-exporter   node节点的CPU、内存利用率
3. kube-state-metrics   pod、deployment、service
4. cadvisor  容器的CPU、内存利用率
5. 

## k8s创建用户并授权

```
apiVersion: v1
kind: ServiceAccount
metadata:
  name: prometheus
  namespace: monitoring

---
apiVersion: v1
kind: Secret
type: kubernetes.io/service-account-token
metadata:
  name: monitoring-token
  namespace: monitoring
  annotations:
    kubernetes.io/service-account.name: "prometheus"
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: prometheus
rules:
- apiGroups:
  - ""
  resources:
  - nodes
  - services
  - endpoints
  - pods
  - nodes/proxy
  verbs:
  - get
  - list
  - watch
- apiGroups:
  - "extensions"
  resources:
    - ingresses
  verbs:
  - get
  - list
  - watch
- apiGroups:
  - ""
  resources:
  - configmaps
  - nodes/metrics
  verbs:
  - get
- nonResourceURLs:
  - /metrics
  verbs:
  - get
---
#apiVersion: rbac.authorization.k8s.io/v1beta1
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: prometheus
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: prometheus
subjects:
- kind: ServiceAccount
  name: prometheus
  namespace: monitoring
```

```
kubectl describe secrets -n monitoring monitoring-token
```

将获取到的token的值存入`k8s.token`中，在prometheus配置文件中会用到

## Prometheus配置文件

```
# my global config
global:
  scrape_interval: 15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
  evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.
  # scrape_timeout is set to the global default (10s).

# Alertmanager configuration
alerting:
  alertmanagers:
    - static_configs:
        - targets:
          # - alertmanager:9093

# Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
rule_files:
  # - "first_rules.yml"
  # - "second_rules.yml"

# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  - job_name: "prometheus"
    static_configs:
      - targets: ["localhost:9090"]

  - job_name: 'file_sd_node_export'
    file_sd_configs:
      - files:
        - /data/prometheus/prometheus-2.53/node.yaml
        refresh_interval: 10s 

  - job_name: 'file_sd_process_export'
    file_sd_configs:
      - files:
        - /data/prometheus/prometheus-2.53/process.yaml
        refresh_interval: 10s

#API Serevr节点发现
  - job_name: 'kubernetes-apiservers'
    kubernetes_sd_configs:
    - role: endpoints
      api_server: https://192.168.196.128:6443
      tls_config: 
        insecure_skip_verify: true  
      bearer_token_file: /data/prometheus/k8s.token 
    scheme: https 
    tls_config: 
      insecure_skip_verify: true 
    bearer_token_file: /data/prometheus/k8s.token 
    relabel_configs:
    - source_labels: [__meta_kubernetes_namespace, __meta_kubernetes_service_name, __meta_kubernetes_endpoint_port_name]
      action: keep
      regex: default;kubernetes;https

#node节点发现
  - job_name: 'kubernetes-nodes-monitor' 
    scheme: http 
    tls_config: 
      insecure_skip_verify: true 
    bearer_token_file: /data/prometheus/k8s.token 
    kubernetes_sd_configs: 
    - role: node 
      api_server: https://192.168.196.128:6443 
      tls_config: 
        insecure_skip_verify: true 
      bearer_token_file: /data/prometheus/k8s.token
    relabel_configs: 
      - source_labels: [__address__] 
        regex: '(.*):10250' 
        replacement: '${1}:9100' 
        target_label: __address__ 
        action: replace 
      - source_labels: [__meta_kubernetes_node_label_failure_domain_beta_kubernetes_io_region] 
        regex: '(.*)' 
        replacement: '${1}' 
        action: replace 
        target_label: LOC 
      - source_labels: [__meta_kubernetes_node_label_failure_domain_beta_kubernetes_io_region] 
        regex: '(.*)' 
        replacement: 'NODE' 
        action: replace 
        target_label: Type 
      #- source_labels: [__meta_kubernetes_node_label_failure_domain_beta_kubernetes_io_region] 
      #  regex: '(.*)' 
      #  replacement: 'K8S-test' 
      #  action: replace 
      #  target_label: Env 
      - action: labelmap 
        regex: __meta_kubernetes_node_label_(.+) 

  - job_name: 'kubernetes-service-endpoints'
    scheme: http 
    tls_config: 
      insecure_skip_verify: true 
    bearer_token_file: /data/prometheus/k8s.token 
    kubernetes_sd_configs: 
    - role: endpoints 
      api_server: https://192.168.196.128:6443 
      tls_config: 
        insecure_skip_verify: true 
      bearer_token_file: /data/prometheus/k8s.token
    relabel_configs:
    - source_labels: [__meta_kubernetes_service_annotation_prometheus_io_scrape]
      action: keep
      regex: true
    - source_labels: [__meta_kubernetes_service_annotation_prometheus_io_scheme]
      action: replace
      target_label: __scheme__
      regex: (https?)
    - source_labels: [__meta_kubernetes_service_annotation_prometheus_io_path]
      action: replace
      target_label: __metrics_path__
      regex: (.+)
    - source_labels: [__address__, __meta_kubernetes_service_annotation_prometheus_io_port]
      action: replace
      target_label: __address__
      regex: ([^:]+)(?::\d+)?;(\d+)
      replacement: $1:$2
    - action: labelmap
      regex: __meta_kubernetes_service_label_(.+)
    - source_labels: [__meta_kubernetes_namespace]
      action: replace
      target_label: kubernetes_namespace
    - source_labels: [__meta_kubernetes_service_name]
      action: replace
      target_label: kubernetes_name
  - job_name: 'kubernetes-pods'
    scheme: http 
    tls_config: 
      insecure_skip_verify: true 
    bearer_token_file: /data/prometheus/k8s.token 
    kubernetes_sd_configs: 
    - role: pod 
      api_server: https://192.168.196.128:6443 
      tls_config: 
        insecure_skip_verify: true 
      bearer_token_file: /data/prometheus/k8s.token
    relabel_configs:
    - source_labels: [__meta_kubernetes_pod_annotation_prometheus_io_scrape]
      action: keep
      regex: true
    - source_labels: [__meta_kubernetes_pod_annotation_prometheus_io_path]
      action: replace
      target_label: __metrics_path__
      regex: (.+)
    - source_labels: [__address__, __meta_kubernetes_pod_annotation_prometheus_io_port]
      action: replace
      regex: ([^:]+)(?::\d+)?;(\d+)
      replacement: $1:$2
      target_label: __address__
    - action: labelmap
      regex: __meta_kubernetes_pod_label_(.+)
    - source_labels: [__meta_kubernetes_namespace]
      action: replace
      target_label: kubernetes_namespace
    - source_labels: [__meta_kubernetes_pod_name]
      action: replace
      target_label: kubernetes_pod_name
  - job_name: "kube-state-metrics"
    static_configs:
      - targets: ["10.96.234.146:8080"]
    metric_relabel_configs:
    - target_label: cluster
      replacement: localzy

```



