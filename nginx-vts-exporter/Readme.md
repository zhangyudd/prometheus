# nginx 监控

[参考1](https://zhuanlan.zhihu.com/p/460300628)

[参考2](https://blog.csdn.net/m0_58523831/article/details/129435949)

## 第三方exporter获取

https://github.com/vozlt/nginx-module-vts/releases

## nginx编译安装模块

```
./configure --add-module=nginx-module-vts-0.2.2 && make 
注意如果是重新编译，不需要make install
```

修改nginx配置

```
vim conf/nginx.conf
......
http {
    ......;
    vhost_traffic_status_zone;
    ......;
    server{
        ......;
        location /status {
            vhost_traffic_status_display;
            vhost_traffic_status_display_format html;
        }
    }
}

https://server/status

https://server/status/format/prometheus
```

## prometheus配置

```
- job_name: nginx
  metrics_path: "/status/format/prometheus"
  static_configs:
    - targets: ['172.30.20.167:9095']

如果nginx配置有修改，重新加载好像无效，需要重启nginx
```



